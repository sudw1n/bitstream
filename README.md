> Note: This library is no longer needed as Zig already provides `std.io.BitWriter` and `std.io.BitReader`.

# BitStream

A tiny library to allow writing to and reading from stream (a `Reader` and a `Writer` in Zig) in bits.

The original credits go to [MichaelDipperstein](https://github.com/MichaelDipperstein) for their implementation of [bitfile](https://github.com/MichaelDipperstein/bitfile). I only implemented it in Zig.
