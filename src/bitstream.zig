const std = @import("std");
const File = std.fs.File;

pub fn BitStream(comptime Writer: type, comptime Reader: type) type {
    return struct {
        const Self = @This();

        writer: Writer,
        reader: Reader,
        bit_count: u8,
        bit_buffer: u8,

        /// Append a bit to the writer. The appended bit will become the lsb of the bit buffer.
        ///
        /// This makes the operations First-In First-Out order.
        pub fn put_bit(self: *Self, c: u8) !void {
            self.bit_count += 1;
            self.bit_buffer <<= 1;

            if (c != 0) {
                self.bit_buffer |= 1;
            }

            // write to file if we have a full byte
            if (self.bit_count == 8) {
                try self.writer.writeByte(self.bit_buffer);
                self.bit_buffer = 0;
                self.bit_count = 0;
            }
        }

        /// Read a bit from the reader. The returned bit is the msb of a byte.
        ///
        /// This makes the operations First-In First-Out order.
        pub fn get_bit(self: *Self) !u8 {
            // read a byte if buffer is empty
            if (self.bit_count == 0) {
                self.bit_buffer = try self.reader.readByte();
                self.bit_count = 8;
            }

            self.bit_count -= 1;
            // return the msb of buffer
            return (self.bit_buffer >> get_shift(self.bit_count)) & 0x1;
        }

        /// Appends the byte to the stream merging it
        /// with the bits already read/written.
        ///
        /// Returns the final value written to the stream.
        pub fn put_byte(self: *Self, byte: u8) !u8 {
            // since we don't have additional bits buffered,
            // we can just write the whole byte
            if (self.bit_count == 0) {
                try self.writer.writeByte(byte);
                return byte;
            }

            // otherwise merge the given byte with what we already
            // have

            // first discard the bits from the byte so that we
            // have room to append bits from out bit_buffer at
            // the beginning
            var tmp = byte >> get_shift(self.bit_count);
            // shift the bit_buffer to the left so that the valid
            // bits get to the beginning, and merge it with the
            // other bits
            tmp |= self.bit_buffer << get_shift(8 - self.bit_count);

            try self.writer.writeByte(tmp);

            // we might not have written everything from `byte`
            // so keep the remaining bits in the buffer
            // how many bits are left is given by bit_count itself
            // so that shouldn't change
            self.bit_buffer = byte;

            // give back what the final value written was
            return tmp;
        }

        /// Return a byte from the stream merging it
        /// with the bits already read/written.
        ///
        /// Returns the final value written to the stream.
        pub fn get_byte(self: *Self) !u8 {
            const ret = try self.reader.readByte();
            // no bits to merge
            if (self.bit_count == 0) {
                return ret;
            }

            // otherwise merge the returned byte from stream with
            // what we already have

            // first discard the bits from the byte so that we
            // have room to append bits from our bit_buffer at
            // the beginning
            var tmp = ret >> get_shift(self.bit_count);
            // shift the bit_buffer to the left so that the valid
            // bits get to the beginning, and merge it with the
            // other bits
            tmp |= self.bit_buffer << get_shift(8 - self.bit_count);

            return tmp;
        }

        /// Appends the given bits to the stream.
        ///
        /// The given count specifies how many bits to write.
        ///
        /// It's assumed that `bits.len` <= `count`
        pub fn put_bits(self: *Self, bits: []const u8, count: usize) !void {
            var offset: usize = 0;
            var remaining: usize = count;

            // writer whole bytes
            while (remaining >= 8) {
                _ = try self.put_byte(bits[offset]);
                remaining -= 8;
                offset += 1;
            }

            if (remaining != 0) {
                // write remaining bits
                var tmp = bits[offset];

                while (remaining > 0) {
                    // write the msb
                    try self.put_bit(tmp & 0x80);
                    // update the msb by the bit next to it
                    tmp <<= 1;
                    remaining -= 1;
                }
            }
        }

        /// Write the given number of bits to `buffer`.
        ///
        /// The given count specifies how many bits to retrieve from the stream.
        ///
        /// It's assumed that `bits.len` <= `count`
        pub fn get_bits(self: *Self, buffer: []u8, count: usize) !void {
            var offset: usize = 0;
            var remaining: usize = count;

            // get whole bytes
            while (remaining >= 8) {
                const ret = try self.get_byte();
                buffer[offset] = ret;
                remaining -= 8;
                offset += 1;
            }

            // write remaining bits
            if (remaining != 0) {
                const shift = get_shift(8 - remaining);
                buffer[offset] = 0;

                while (remaining > 0) {
                    const ret = try self.get_bit();
                    buffer[offset] <<= 1;
                    buffer[offset] |= ret;
                    remaining -= 1;
                }

                // shift the bits into position
                buffer[offset] <<= shift;
            }
        }

        /// Flush out the bit buffer to the stream
        /// if any bits remain in it.
        ///
        /// The bit buffer is aligned to the nearest byte.
        ///
        /// Returns the byte aligned value if any bits were
        /// remaining in the bit buffer.
        pub fn flush(self: *Self) !?u8 {
            // write out any remaining bits
            if (self.bit_count != 0) {
                self.bit_buffer <<= get_shift(8 - self.bit_count);
                try self.writer.writeByte(self.bit_buffer);
                return self.bit_buffer;
            }

            self.bit_buffer = 0;
            self.bit_count = 0;
            return null;
        }
    };
}

pub fn bitStream(writer: anytype, reader: anytype) BitStream(@TypeOf(writer), @TypeOf(reader)) {
    return .{
        .writer = writer,
        .reader = reader,
        .bit_count = 0,
        .bit_buffer = 0,
    };
}

inline fn get_shift(num: anytype) u3 {
    return switch (@typeInfo(@TypeOf(num))) {
        .Int, .ComptimeInt => @as(u3, @truncate(num)),
        else => @compileError("get_shift() only supports integers"),
    };
}

// ==================== TESTS ====================

test "write bit" {
    var buffer: [0x10]u8 = undefined;
    var stream = std.io.fixedBufferStream(&buffer);

    const writer = stream.writer();
    const reader = stream.reader();

    var bitfile = bitStream(writer, reader);

    try bitfile.put_bit(1);
    try bitfile.put_bit(0);
    try bitfile.put_bit(1);
    try bitfile.put_bit(1);
    try bitfile.put_bit(1);
    try bitfile.put_bit(1);
    try bitfile.put_bit(1);

    // we're about to reach a whole byte
    try std.testing.expect(bitfile.bit_count == 7);
    try std.testing.expect(bitfile.bit_buffer == 0b01011111);

    try bitfile.put_bit(1);

    // bit buffer should be empty and we should have a byte in our stream
    try std.testing.expect(bitfile.bit_count == 0);
    try std.testing.expect(bitfile.bit_buffer == 0);

    try std.testing.expect(buffer[0] == 0b10111111);
}

test "read bit" {
    var buffer: [0x10]u8 = undefined;
    var stream = std.io.fixedBufferStream(&buffer);

    const writer = stream.writer();
    const reader = stream.reader();

    const original = 0b10111110;
    try writer.writeByte(original);

    stream.reset();

    var bitfile = bitStream(writer, reader);

    var read_value: u8 = 0;

    for (0..8) |_| {
        read_value <<= 1;
        read_value |= try bitfile.get_bit();
    }

    // we should've read a complete byte by now
    try std.testing.expect(bitfile.bit_count == 0);

    // should get back the original value stored in the underlying stream
    try std.testing.expect(read_value == original);
}

test "put_byte() and get_byte() returns original byte if no remaining bits" {
    var buffer: [0x10]u8 = undefined;
    var stream = std.io.fixedBufferStream(&buffer);

    const writer = stream.writer();
    const reader = stream.reader();

    var bitfile = bitStream(writer, reader);

    const original = 0b00111110;
    // we shouldn't have any bits to merge so
    // returned value should be same as original
    try std.testing.expect(try bitfile.put_byte(original) == original);
}

test "put_byte() and get_byte() merges remaining bits" {
    var buffer: [0x10]u8 = undefined;
    var stream = std.io.fixedBufferStream(&buffer);

    const writer = stream.writer();
    const reader = stream.reader();

    const original = 0b00111110;
    try writer.writeByte(original);

    stream.reset();

    var bitfile = bitStream(writer, reader);

    try bitfile.put_bit(0b1);

    // the previous bit should be merged with `original`
    try std.testing.expect(try bitfile.get_byte() == 0b10011111);
}

test "write bits" {
    var buffer: [0x10]u8 = undefined;
    var stream = std.io.fixedBufferStream(&buffer);

    const writer = stream.writer();
    const reader = stream.reader();

    var bitfile = bitStream(writer, reader);

    // convert a u9 0b100000000 (1 followed by 8 0s)
    // into [2]u8 { 0b00000001, 0b00000000 }
    const original = @bitReverse(@as(u9, 0xff + 1));
    const bytes: [*]const u8 = @ptrCast(&original);

    try bitfile.put_bits(bytes[0..2], 9);

    // should've written a whole byte
    try std.testing.expect(buffer[0] == 0b00000001);

    // should have one bit left
    try std.testing.expect(bitfile.bit_count == 1);
}

test "read bits" {
    var buffer: [0x10]u8 = undefined;
    var stream = std.io.fixedBufferStream(&buffer);

    const writer = stream.writer();
    const reader = stream.reader();

    var bitfile = bitStream(writer, reader);

    const original = @bitReverse(@as(u17, 0xffff + 1));
    const bytes: [*]const u8 = @ptrCast(&original);

    try writer.writeAll(bytes[0..3]);
    stream.reset();

    var got: [3]u8 = undefined;
    try bitfile.get_bits(got[0..3], 17);

    try std.testing.expect(std.mem.eql(u8, &got, bytes[0..3]));
}

test "flush" {
    var buffer: [0x10]u8 = undefined;
    var stream = std.io.fixedBufferStream(&buffer);

    const writer = stream.writer();
    const reader = stream.reader();

    var bitfile = bitStream(writer, reader);

    try bitfile.put_bit(1);
    try bitfile.put_bit(0);
    try bitfile.put_bit(1);

    const flushed = try bitfile.flush();

    try std.testing.expect(flushed == 0b10100000);
    try std.testing.expect(buffer[0] == flushed);
}
