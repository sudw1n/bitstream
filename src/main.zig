const std = @import("std");
const bitstream_lib = @import("bitstream.zig");
pub const BitStream = bitstream_lib.BitStream;
pub const bitStream = bitstream_lib.bitStream;

test {
    std.testing.refAllDecls(@This());
}
