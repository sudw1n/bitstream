const std = @import("std");

pub fn build(b: *std.Build) void {
    const target = b.standardTargetOptions(.{});

    const optimize = b.standardOptimizeOption(.{});

    const lib = b.addStaticLibrary(.{
        .name = "bitstream",
        .root_source_file = .{ .path = "src/main.zig" },
        .target = target,
        .optimize = optimize,
    });

    const bitstream_mod = b.addModule("bitstream", .{
        .source_file = .{ .path = "src/main.zig" },
        .dependencies = &.{},
    });

    lib.addModule("bitstream", bitstream_mod);

    b.installArtifact(lib);

    const install_docs = b.addInstallDirectory(.{
        .source_dir = lib.getEmittedDocs(),
        .install_dir = .prefix,
        .install_subdir = "docs",
    });

    const docs_step = b.step("docs", "Copy documentation artifacts to prefix path");
    docs_step.dependOn(&install_docs.step);

    const test_step = b.step("test", "Run library tests");

    const test_targets = [_]std.zig.CrossTarget{
        .{}, // native
        .{ .cpu_arch = .x86_64, .os_tag = .linux, .abi = .musl },
        .{ .cpu_arch = .x86_64, .os_tag = .linux, .abi = .gnu },
        .{ .cpu_arch = .aarch64, .os_tag = .linux, .abi = .musl },
        .{ .cpu_arch = .aarch64, .os_tag = .linux, .abi = .gnu },
    };

    // test for all of the cross-targets
    for (test_targets) |test_target| {
        const tests = b.addTest(.{
            .root_source_file = .{ .path = "src/main.zig" },
            .target = test_target,
            .optimize = optimize,
        });
        const run_tests = b.addRunArtifact(tests);
        run_tests.skip_foreign_checks = true;

        test_step.dependOn(&run_tests.step);
    }
}
